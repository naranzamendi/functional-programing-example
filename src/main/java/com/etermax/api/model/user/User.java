package com.etermax.api.model.user;

/**
 * Created by magodeoz on 26/10/16.
 */
public class User {

    private String username;
    private String email;

    public User(String username, String email){
        this.username = username;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }
}
