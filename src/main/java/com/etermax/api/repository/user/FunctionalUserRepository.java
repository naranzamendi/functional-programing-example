package com.etermax.api.repository.user;

import com.etermax.api.cache.user.UserCache;
import com.etermax.api.model.user.User;

import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created by magodeoz on 26/10/16.
 */
public class FunctionalUserRepository {

    private UserCache userCache;
    private UserDao userDao;
    private Function<String, BiFunction<Function<String, User>, Function<String, User>, User>> findUserFx = key ->
                                                                                                                (findInCacheFx, findInDBFx) ->
                                                                                                                                           findByKeyFromCacheFallbackDB(key, findInCacheFx, findInDBFx);

    public FunctionalUserRepository(UserDao userDao, UserCache userCache){
        this.userDao = userDao;
        this.userCache = userCache;
    }

    public User findByEmail(String email){
        return findUserFx.apply(email)
                         .apply(userCache::get, userDao::findByEmail);
    }

    public User findByUsername(String username){
        return findUserFx.apply(username)
                       .apply(userCache::get, userDao::findByUsername);
    }

    private User findByKeyFromCacheFallbackDB(String key, Function<String, User> findInCache, Function<String, User> findInDB) {
        return Optional.ofNullable(findInCache.apply(key))
                       .orElse(findInDB.andThen(user -> userCache.save(key, user))
                                         .apply(key));
    }

    public void save(User user){
        userDao.save(user);
        saveInCache(user);
    }


    private void saveInCache(User user) {
        userCache.save(user.getUsername(), user);
        userCache.save(user.getEmail(), user);
    }

}
