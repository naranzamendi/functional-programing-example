package com.etermax.api.repository.user;

import com.etermax.api.cache.user.UserCache;
import com.etermax.api.model.user.User;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * Created by magodeoz on 26/10/16.
 */
public class UserRepository {

    private UserCache userCache;
    private UserDao userDao;

    public UserRepository(UserDao userDao, UserCache userCache){
        this.userDao = userDao;
        this.userCache = userCache;
    }

    public User findByEmail(String email){

        User user = findInCacheByEmail(email);

        if(user == null){
            user = userDao.findByEmail(email);
            saveInCache(user);
        }

        return user;
    }

    public User findByUsername(String username){

        User user = findInCacheByUsername(username);

        if(user == null){
            user = userDao.findByUsername(username);
            saveInCache(user);
        }

        return user;
    }


    public void save(User user){
        userDao.save(user);
        saveInCache(user);
    }

    private User findInCacheByEmail(String email){
        return userCache.get(email);
    }

    private User findInCacheByUsername(String username){
        return userCache.get(username);
    }

    private void saveInCache(User user) {
        userCache.save(user.getUsername(), user);
        userCache.save(user.getEmail(), user);
    }

}
